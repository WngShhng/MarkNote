package me.shouheng.notepal.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;

import java.util.Objects;

import me.shouheng.data.entity.Attachment;
import me.shouheng.notepal.Constants;
import me.shouheng.notepal.R;
import me.shouheng.notepal.activity.GalleryActivity;
import me.shouheng.notepal.manager.FileManager;
import me.shouheng.utils.ui.ViewUtils;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * The image fragment to display the image, video preview image etc.
 *
 * Created by WngShhng (shouheng2015@gmail.com) on 2017/4/9.
 */
public class ImageFragment extends Fragment {

    public static final String ARG_ATTACHMENT = "__args_key_attachment";

    private static final String STATE_SAVE_KEY_ATTACHMENT = "__state_save_key_attachment";

    private Attachment attachment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(ARG_ATTACHMENT)){
            attachment = (Attachment) getArguments().get(ARG_ATTACHMENT);
        }
        if (savedInstanceState != null){
            attachment = savedInstanceState.getParcelable(STATE_SAVE_KEY_ATTACHMENT);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (attachment != null && Constants.MIME_TYPE_VIDEO.equals(attachment.getMineType())){
            RelativeLayout layout = new RelativeLayout(getContext());
            ImageView imageView = new ImageView(getContext());
            imageView.setLayoutParams(new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
            layout.addView(imageView);
            ImageView videoTip = new ImageView(getContext());
            videoTip.setImageResource(R.drawable.ic_play_circle_outline_white_24dp);
            int dp50 = ViewUtils.dp2px(50);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(dp50, dp50);
            params.addRule(RelativeLayout.CENTER_IN_PARENT);
            videoTip.setLayoutParams(params);
            layout.addView(videoTip);
            displayMedia(imageView);
            return layout;
        }

        PhotoView photoView = new PhotoView(Objects.requireNonNull(getContext()));
        if (attachment != null && "gif".endsWith(attachment.getUri().getPath())) {
            Glide.with(Objects.requireNonNull(getActivity())).load(attachment.getUri().getPath()).into(photoView);
        } else {
            displayMedia(photoView);
        }
        photoView.setOnPhotoTapListener((view, x, y) -> {
            Activity activity = getActivity();
            if (activity instanceof GalleryActivity) {
                ((GalleryActivity) activity).toggleSystemUI();
            }
        });
        photoView.setMaximumScale(5.0F);
        photoView.setMediumScale(3.0F);
        return photoView;
    }

    private void displayMedia(PhotoView photoView) {
        Glide.with(Objects.requireNonNull(getContext()))
                .load(FileManager.getThumbnailUri(getContext(), attachment.getUri()))
                .thumbnail(0.5f)
                .transition(withCrossFade())
                .into(photoView);
        photoView.setOnClickListener(v -> {
            if (attachment != null && Constants.MIME_TYPE_VIDEO.equals(attachment.getMineType())){
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.setDataAndType(attachment.getUri(), FileManager.getMimeType(getContext(), attachment.getUri()));
                startActivity(intent);
            }
        });
    }

    /**
     * Note that you shouldn't set diskCacheStrategy of Glide, and you should use ImageView instead of PhotoView
     *
     * @param imageView view to show
     */
    private void displayMedia(ImageView imageView){
        Glide.with(Objects.requireNonNull(getContext()))
                .load(FileManager.getThumbnailUri(getContext(), attachment.getUri()))
                .transition(withCrossFade())
                .into(imageView);
        imageView.setOnClickListener(v -> {
            if (attachment != null && Constants.MIME_TYPE_VIDEO.equals(attachment.getMineType())){
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.setDataAndType(attachment.getUri(), FileManager.getMimeType(getContext(), attachment.getUri()));
                startActivity(intent);
            }
        });
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable(STATE_SAVE_KEY_ATTACHMENT, attachment);
        super.onSaveInstanceState(outState);
    }
}
